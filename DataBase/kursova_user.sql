CREATE DATABASE  IF NOT EXISTS `kursova` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kursova`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: kursova
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Uname` varchar(45) NOT NULL,
  `passwordhash` varchar(500) DEFAULT NULL,
  `role_idrole` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`,`Uname`),
  KEY `fk_user_role1_idx` (`role_idrole`),
  CONSTRAINT `fk_user_role1` FOREIGN KEY (`role_idrole`) REFERENCES `role` (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (32,'admin','dSPGKr23Yoxana2Pl9jYxcBA7eNlNeUxqKN0i2yufgA=',1),(34,'admin24','JtaorZfHX/xUj2hz5ek85HVHnj4aEJc4HlQiH7U+wdI=',2),(36,'kopo','ZVfbBLrl7rjWxE2on4Lxy7VHLHPxXqucsVPf+7pXVbA=',2),(37,'joli','zMtrxspvNfbvaKhp2UDSrNw1JFWzS4tOlGXSKa1TRlA=',2),(38,'dog','5sIkHYpMo1UrJuDS1Db+/lAYgLs/CMAfv8iMlipkMYk=',2),(39,'kolk','aNjX4/owbRxm8xqMFdSCOGL63ioiX9u1AIVcARc5vZ8=',2),(40,'dodik','RvPfpqv/ryKpZDt9FU1mit84u4CnoC0KfSarXu7W+W0=',2),(41,'lplp','bsPVlqkBsqSr9TuybFOBrbNZF1klNX0Ouwr5Jf92Omc=',2),(42,'lplp1','NeUJ/RmScB7rWRcxABFHS1piOd8cuzhBLglB87cw1OE=',2),(43,'molk','F+HGpNmuJ6K35gqQ8wyibUvAqDeP2QzNlxj1UMLE42c=',2),(45,'popo','fK8Ov05paFHLQynSiyzn3/DKL46ZrFThqAtj3fvMtOw=',2),(47,'jojo','jkT9XPVEqr8Z56GpIP2fVJ/BL4RCO/3hVCi/IS51bC0=',2),(48,'nnn','oLboYy5YKdSXQRRKVcNZipkSqXw/2cOi3X1j17ht4pA=',2),(49,'mmm','RKDYU0YN6mLPigVv841oIVlaVeH9TqSIhkxKDAkCXoQ=',2),(50,'bb','mOuQcPloRJTak2E1CgQBdy1fu01EHVDvKsIWt0ramQE=',2),(51,'mbm','yi7rxJLSxn4piEBhrXSYP3h6niTkZeFuDKI0hFhcSgw=',2),(53,'yyy','d8odhb292w3TkRe6FRdfFY+tBH30c6R7v/b0ugEzD+A=',2),(54,'ijiji','I4Vo4LrBi3gTyBckyLAhlZ9CFQrzIy/l5BOYUIQSPfI=',2),(55,'hjk','hyXE1Jxhq+0xEt2KyCPNyVO5L9qDiQuNviv+530fSVY=',2),(56,'jj','IQdTd1dCNbE+96BCOWctFQePodtG9VdfRsuVA/5fuEg=',2),(58,'uuu','F4nVcmvLORuRubDq6hKo/pbEtDGQRzhKbahv9ZJlUnM=',2),(59,'hj','PwJ6dLnvngmyEaD+bvB/ndEg9tna0gDRKKZlyf/SDB4=',2),(60,'ghj','nyms8DXy+h6wyVhx3VpFJHvrFunJYI4Kab89PkTuOIU=',2),(61,'jkjj','gYHD72d43lZ7fB/sAwLF67kYMdoiYOS2kk+Az+LBmGg=',2),(62,'bbbn','IkgT1H6v2jfdfHPv8aB6HuCcriNgiLYGDXrCNXLNWYM=',2),(63,'jk','CpytTgTcSi0bzFW8YV/r5QZtudQVZzvd+zcfOj9+Pqk=',2),(64,'nmnm','wpg0wbYGKf9E5x1WMCMxMrY7silk/dLLWOg2/HGAd5w=',2),(65,'klkl','46+i7/SsCPQbVDmmqxvE3OY6vzppclPFjrBZIKMdn+I=',2),(66,'kkk','/dHTQrpXWn+efdZeEV0n5TIFBjKbj1dqwME3NoDDzfU=',2),(67,'hjhj','kEnPbZ/PSBWwvbuYFHovhOSPvS2qEmPbXWLIdfPzmHY=',2),(68,'kokoko','26ti6GwDvyi7ewRhttm/lRgnP1AQiwTmKAQnRv9C/Sk=',2),(69,'adminNew','JtaorZfHX/xUj2hz5ek85HVHnj4aEJc4HlQiH7U+wdI=',2),(70,'kkkk','z+LdcZ/09TOdxYzaa43eaP90SthXgEz4GZtoes9MXVo=',2),(71,'mnmn','KSCB3cFbY/918+IDzgV13wifX7QdX5RQlkpn32GHAXc=',2),(72,'cfcf','7m9H41g6oYVCXl0t1GNgsC+YWg6Iqo0zDFw7WLv8NYw=',2),(73,'pokerman','G9fwbteUuyaU2H1uFt+QXhTDfLoAB7Jf5Q7YWZvOV1E=',2),(74,'boyardev','dUKLLvXjGFfPqnrr4Zhv10a6FkXOLixnUrmvs489ZXw=',2),(76,'pokk','CrMlQD+g5g3aB19gQcMRDRHoTGgcw/TqiVng+q/Mzro=',2),(77,'pokklk','sgGI4ojf5FbvmWUYbgZpJZREH7nNAdyZzc5G/S24Ju8=',2),(78,'kklk','zOKUs+E090Dm0gw3/pFJB60wraTR3Nz8IC2edh0FwSw=',2),(79,'popoo','h7zfsAYr+55J3UuiKbYq5bjrXfLrrIrYmst0yyC0OjE=',2),(81,'vadim','BunMMsRhW19+1D+Xi+Bu1lsBuL3u0+CtaC4pC0e+p4M=',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `user_AFTER_INSERT` AFTER INSERT ON `user` FOR EACH ROW BEGIN
INSERT INTO `kursova`.`images` (`imname`, `imhash`, `user_ID`, `user_Uname`, `status`) VALUES ('default', load_file('C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/default.png'), new.ID, new.Uname, '1');
INSERT INTO `kursova`.`userdata` (`user_ID`, `user_Uname`) VALUES (new.ID, new.Uname);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-17 16:19:27
