CREATE DATABASE  IF NOT EXISTS `kursova` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kursova`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: kursova
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `userdata`
--

DROP TABLE IF EXISTS `userdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata` (
  `user_ID` int(11) NOT NULL,
  `user_Uname` varchar(45) NOT NULL,
  `FName` varchar(45) DEFAULT 'John',
  `LName` varchar(45) DEFAULT 'Dou',
  `PNumber` varchar(45) DEFAULT 'Unknown',
  `Adress` varchar(45) DEFAULT 'Unknown',
  `Gender` enum('Male','Female','Unknown') DEFAULT 'Unknown',
  `Age` int(11) DEFAULT '18',
  PRIMARY KEY (`user_Uname`),
  KEY `fk_UserData_user1_idx` (`user_ID`,`user_Uname`),
  CONSTRAINT `fk_UserData_user1` FOREIGN KEY (`user_ID`, `user_Uname`) REFERENCES `user` (`id`, `uname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdata`
--

LOCK TABLES `userdata` WRITE;
/*!40000 ALTER TABLE `userdata` DISABLE KEYS */;
INSERT INTO `userdata` VALUES (32,'admin','Dima','Stiopa','38093643432234','SHept 134','Male',18),(69,'adminNew','Admin','Adminchenko','88005553555','Kiev','Male',90),(50,'bb','John','Dou','89899','Unknown','Unknown',18),(62,'bbbn','John','Dou','Unknown','Unknown','Unknown',18),(74,'boyardev','John','Dou','Unknown','Unknown','Unknown',18),(72,'cfcf','cfcf','cfcf','cfcf','cfcf','Male',12),(40,'dodik','Dodik','Dodikovich','39493','Dodika 10','Unknown',45),(60,'ghj','John','Dou','Unknown','Unknown','Unknown',18),(59,'hj','John','Dou','Unknown','Unknown','Unknown',18),(67,'hjhj','John','Dou','Unknown','Unknown','Unknown',18),(55,'hjk','John','Dou','Unknown','Unknown','Unknown',18),(54,'ijiji','John','Dou','Unknown','Unknown','Unknown',18),(56,'jj','jk','jk','jk','jk','Male',18),(63,'jk','John','Dou','Unknown','Unknown','Unknown',18),(61,'jkjj','John','Dou','Unknown','Unknown','Unknown',18),(47,'jojo','doges','catyss','43243242322','4434342422','Male',99),(66,'kkk','John','Dou','Unknown','Unknown','Unknown',18),(70,'kkkk','yfudsgfhs','dhgfjdshjgf','fjdghdfjkg','dfjghdfkg','Male',12),(78,'kklk','John','Dou','Unknown','Unknown','Unknown',18),(65,'klkl','John','Dou','Unknown','Unknown','Unknown',18),(68,'kokoko','klkl','klkl','klkl','klkl','Male',12),(42,'lplp1','Ronald','MacDonald','17032417','MacDonald`s','Unknown',65),(51,'mbm','John','Dou','Unknown','Unknown','Unknown',18),(49,'mmm','John','Dou','Unknown','Unknown','Unknown',18),(71,'mnmn','Mon','Mon','909090','Mon1`9','Unknown',19),(43,'molk','John','Dou','Unknown','Unknown','Unknown',18),(64,'nmnm','John','Dou','Unknown','Unknown','Unknown',18),(48,'nnn','John','Dou','Unknown','Unknown','Unknown',18),(73,'pokerman','Dmitriy','Stiopa','9090888','Ternopil','Male',18),(76,'pokk','John','Dou','Unknown','Unknown','Unknown',18),(77,'pokklk','John','Dou','Unknown','Unknown','Unknown',18),(45,'popo','Popik','Popich','8800','popi','Male',17),(79,'popoo','John','Dou','Unknown','Unknown','Unknown',18),(58,'uuu','Johnsd','Doudsd','Unknownsds','Unknowndsd','Male',188),(81,'vadim','Вадим','Єпур','38097878','Баштанка Сіті','Female',19),(53,'yyy','Tester','Test','8989','8989','Male',89);
/*!40000 ALTER TABLE `userdata` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-17 16:19:28
