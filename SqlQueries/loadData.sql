USE `kursova`;
drop procedure if exists LoadPosts;
DELIMITER //
USE `kursova`//
CREATE PROCEDURE `LoadPosts` (IN user VARCHAR(45))
BEGIN
Select u.Uname, u.ID, p.postTitle, p.postText, p.postID
FROM kursova.user as u 
Join kursova.posts as p on u.ID = p.user_ID;
END//
