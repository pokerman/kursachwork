USE `kursova`;
drop procedure if exists LoadDataToListView;
DELIMITER //
USE `kursova`//
CREATE PROCEDURE `LoadDataToListView` (IN search VARCHAR(45), IN exeption varchar(45))
IN user_ID INT, 
IN imhash VARCHAR(200),
IN FName varchar(45),
IN LName varchar(45),
IN Address varchar(45),
IN Age INT,
IN Gender enum('Male', 'Female', 'Unknown'),
IN PNumber INT)
BEGIN
Select u.Uname, u.ID, i.imname, i.imhash, r.rname 
FROM kursova.user as u 
Join kursova.images as i on u.ID = i.user_ID 
Join kursova.role as r on u.role_idrole = r.idrole 
Where i.imhash is not null and u.Uname Like search and u.Uname != exeption;
END//
