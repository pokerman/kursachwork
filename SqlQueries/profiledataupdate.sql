USE `kursova`;
drop procedure if exists profiledataupdate;
DELIMITER //
USE `kursova`//
CREATE PROCEDURE `profiledataupdate` (IN username VARCHAR(45),
IN user_ID INT, 
IN imhash BLOB,
IN FName varchar(45),
IN LName varchar(45),
IN Address varchar(45),
IN Age INT,
IN Gender enum('Male', 'Female', 'Unknown'),
IN PNumber VARCHAR(45))
BEGIN
UPDATE images i, userdata d
SET i.imhash = CASE
	WHEN imhash IS NULL THEN i.imhash
    ELSE imhash
END,
d.FName = FName, d.LName = LName, d.Adress = Address, d.Age = Age, d.Gender = Gender, d.PNumber = PNumber
WHERE d.user_Uname= username and i.user_Uname = username and d.user_ID = user_ID and i.user_ID = user_ID;
END//
