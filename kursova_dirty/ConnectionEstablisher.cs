﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace kursova_dirty
{
    class ConnectionEstablisher
    {

        const string connData = "server=localhost;user id=root;database=kursova;password=123456788;SslMode=none";

        public MySqlCommand Openconn(string request)
        {
            MySqlConnection conn = new MySqlConnection(connData);
            conn.Open();
            MySqlCommand command = new MySqlCommand(request, conn);
            command.ExecuteNonQuery();
            return command;

        }
        public MySqlDataAdapter Openadapter(string request)
        {
            MySqlConnection conn = new MySqlConnection(connData);
            conn.Open();
            MySqlDataAdapter thisAdapter = new MySqlDataAdapter(request, conn);
            
            return thisAdapter;
        }
        public void Closeconn()
        {
            MySqlConnection conn = new MySqlConnection(connData);
            conn.Close();
        }
    }
}
