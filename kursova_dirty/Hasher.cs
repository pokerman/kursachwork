﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kursova_dirty
{
    class Hasher
    {
        public string Hash(string password)
        {
            byte[] epass = Encoding.Unicode.GetBytes(password);
            var hasher = System.Security.Cryptography.SHA256.Create();
            var passhash = hasher.ComputeHash(epass);
            var hashedString = Convert.ToBase64String(passhash);
            return hashedString;
        }
    }
}
