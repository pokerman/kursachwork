﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kursova_dirty
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            Instance = this;
        }

        ConnectionEstablisher conn = new ConnectionEstablisher();
        public static Login Instance;
        public int userid { get; set; }
        private string username;
        public string Username
        {
            get
            {
                return this.waterMarkTextBox1.Text;
            }
            set
            {
                this.username = value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var sqlhash = string.Empty;
            this.username = Username;
            var password = waterMarkTextBox2.Text;
            byte[] epass = Encoding.Unicode.GetBytes(password);
            var hasher = System.Security.Cryptography.SHA256.Create();
            var passhash = hasher.ComputeHash(epass);
            var hashedString = Convert.ToBase64String(passhash);
            string request = "SELECT passwordhash,ID FROM kursova.user WHERE Uname =" + "'" + this.username + "'";
            var command = conn.Openconn(request);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sqlhash = reader.GetString("passwordhash").ToString();
                userid = reader.GetInt32("ID");
            }
            conn.Closeconn();
            if (hashedString == sqlhash)
            {
                Main form = new Main();
                form.Show();
                this.Close();
            }
            else
                MessageBox.Show("Wrong password or username!");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            UserCreate form = new UserCreate();
            form.ShowDialog();
            
        }

    }

}
