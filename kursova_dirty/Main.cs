﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.IO;


namespace kursova_dirty
{
    public partial class Main : Form
    { 
        public Main()
        {
            InitializeComponent();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            this.listView1.View = View.LargeIcon;
            this.imageList1.ImageSize = new Size(42, 42);
            this.listView1.LargeImageList = this.imageList1;
            LoadLogUser();
            LoadData();
            InstanceM = this;
            button1.Enabled = false;
            button2.Enabled = false;
        }

        string path = null;
        bool permission = false;
        string searchstr;
        public static Main InstanceM;
        private string username = Login.Instance.Username;
        public int Userid { get; set; }
        int id;

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }

        ConnectionEstablisher conn = new ConnectionEstablisher();
        public ObservableCollection<DataRowView> Rows { get; set; }
        private string uname { get; set; }

        //Loading current user data
        private void LoadLogUser()
        {
            string request = "call sdataload ('" + Username + "')";
            var command = conn.Openconn(request);
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {   
                label7.Text = reader.GetString("FName") + " " + reader.GetString("LName");
                linkLabel2.Text = reader.GetString("rname");

                if (reader.GetString("rname") == "admin")
                    this.permission = true;

                byte[] byteBLOBData = (byte[])reader["imhash"];
                var stream = new MemoryStream(byteBLOBData);
                Image image = Image.FromStream(stream);
                pictureBox1.Image = image;
                this.Userid = reader.GetInt32("ID");
                this.Username = reader.GetString("Uname");
                linkLabel1.Text = this.Username;
            }
            conn.Closeconn();
        }

        //Load database to listview
        private void LoadData()
        {
            //calling procedure with search string
            string request = "call LoadDataToListView ('" + searchstr + "%', '" + this.Username + "')";
            var adapter = conn.Openadapter(request);

            DataTable dt = new DataTable();
            adapter.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["Uname"].ToString());
                listitem.SubItems.Add(dr["Uname"].ToString());
                byte[] byteBLOBData = (byte[])dr["imhash"];
                var stream = new MemoryStream(byteBLOBData);
                Image image = Image.FromStream(stream);
                imageList1.Images.Add(image);
                listView1.Items.Add(listitem.Text, imageList1.Images.Count - 1);
            }
            conn.Closeconn();
        }

        //Load selected user profile to right menu
        private void LoadSelectedUserProfile()
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button8.Enabled = true;
            EditBoxStatus(false);
            foreach (ListViewItem itm in listView1.SelectedItems)
            {
                int imgIndex = itm.ImageIndex;
                if (imgIndex >= 0 && imgIndex < this.imageList1.Images.Count)
                {
                    uname = this.listView1.Items[imgIndex].Text;
                    label1.Text = uname;
                }
            }
            string request = "call sdataload ('" + uname + "')";
            try
            {
                var command = conn.Openconn(request);
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    label2.Text = reader.GetString("FName") + " " + reader.GetString("LName");
                    label3.Text = reader.GetString("PNumber");
                    label4.Text = reader.GetString("Adress");
                    label5.Text = reader.GetString("Gender");
                    label6.Text = reader.GetString("Age");
                    this.id = reader.GetInt32("ID");
                    byte[] byteBLOBData = (byte[])reader["imhash"];
                    var stream = new MemoryStream(byteBLOBData);
                    Image image = Image.FromStream(stream);
                    pictureBox2.Image = image;
                    LoadLastPost();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.ToString());
            }
            finally
            {
                conn.Closeconn();
                Main.InstanceM.Update();
            }
            
        }

        //Load data for edit
        private void LoadEditData()
        {
            string request = "call sdataload ('" + uname + "')";
            var command = conn.Openconn(request);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                label1.Text = reader.GetString("Uname");
                textBox1.Text = reader.GetString("FName");
                textBox2.Text = reader.GetString("LName");
                textBox4.Text = reader.GetString("PNumber");
                textBox3.Text = reader.GetString("Adress");
                comboBox1.Text = reader.GetString("Gender");
                textBox5.Text = reader.GetString("Age");
                var byteBLOBData = (byte[])reader["imhash"];
                var stream = new MemoryStream(byteBLOBData);
                Image image = Image.FromStream(stream);
                pictureBox2.Image = image;
            }
            conn.Closeconn();
        }


        //Updating...
        public void LoadParent()
        {
            pictureBox1.InitialImage = null;
            imageList1.Images.Clear();
            listView1.Clear();
            LoadData();
            LoadLogUser();
        }

        //Load selected profile in listview
        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            LoadSelectedUserProfile();
        }

        //Open Profile Form
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Profile form = new Profile();
            form.Show();
        }

        //Search
        private void waterMarkTextBox1_TextChanged(object sender, EventArgs e)
        {
            searchstr = waterMarkTextBox1.Text;
            imageList1.Images.Clear();
            listView1.Clear();
            LoadData();
        }

        //User edit {admin rights}
        private void button1_Click(object sender, EventArgs e)
        {
            if (permission == true)
            {
                EditBoxStatus(true);
                LoadEditData();
            }
            else
                MessageBox.Show("Permission Denied!");
        }

        //Deleting user {admin rights}
        private void button2_Click(object sender, EventArgs e)
        {
            var confirmresult = MessageBox.Show("Are you sure you want to delete this user?", "Confirm Delete", MessageBoxButtons.YesNo);
            if (confirmresult == DialogResult.Yes && this.permission == true)
            {
                string request = "call userdelete('" + this.uname + "', " + this.id + ")";
                conn.Openconn(request);
                conn.Closeconn();
                MessageBox.Show("User Deleted Successfully");
                LoadParent();
            }
            else if (confirmresult == DialogResult.Yes && this.permission == false)
                MessageBox.Show("Permission Denied!");
        }

        //Permission box
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (linkLabel2.Text == "admin")
                MessageBox.Show("You have all the rights!", "Rights");
            else
                MessageBox.Show("You have default user rights", "Rights");
        }

        //Edit Interface Control
        private void EditBoxStatus (bool status)
        {
            button3.Visible = status;
            button4.Visible = status;
            button5.Visible = status;
            textBox1.Visible = status;
            textBox2.Visible = status;
            textBox3.Visible = status;
            textBox4.Visible = status;
            textBox5.Visible = status;
            comboBox1.Visible = status;
        }

        //Cancel Edit
        private void button5_Click(object sender, EventArgs e)
        {
            EditBoxStatus(false);
        }

        //Select user image {edit menu}
        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            PictureBox mainpic = new PictureBox();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                mainpic.Image = Image.FromFile(fileDialog.FileName);
                this.path = fileDialog.FileName;
                this.path = this.path.Replace(@"\", "/");
                pictureBox2.Image = Image.FromFile(fileDialog.FileName);
            }
        }

        //Save userdata changes {edit menu}
        private void button4_Click(object sender, EventArgs e)
        {
            string request = "call profiledataupdate('" + this.uname + "', " + this.id + "," +
            " load_file('" + path + "'), '" + textBox1.Text + "'," +
            " '" + textBox2.Text + "', '" + textBox3.Text + "'," +
            " " + textBox5.Text + ", '" + comboBox1.SelectedItem.ToString() + "', '" + textBox4.Text + "')";
            try
            {
                conn.Openconn(request);
               // conn.Closeconn();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Too big Image");
            }
            finally
            {
                conn.Closeconn();
            }
            LoadSelectedUserProfile();
            LoadParent();
        }

        private void Loadposts()
        {
            var request = "call LoadPosts('" + uname + "')";
            var adapter = conn.Openadapter(request);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            listBox1.DataSource = dt;
            listBox1.DisplayMember = "postTitle";
            listBox1.ValueMember = "postID";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Postspage form = new Postspage();
            form.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            listBox1.Visible = true;
            button9.Visible = true;
            richTextBox2.Visible = true;
            Loadposts();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            listBox1.DataBindings.Clear();
            listBox1.Visible = false;
            button9.Visible = false;
            richTextBox2.Visible = false;
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                int id = int.TryParse(listBox1.SelectedValue.ToString(), out id) ? id : 0;
                var request = "Select postText from kursova.posts Where postID = " + id;
                var command = conn.Openconn(request);
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    richTextBox2.Text = reader.GetString("postText");
                }
            }
            catch
            {

            }
            finally
            {
                conn.Closeconn();
            }
        }
        private void LoadLastPost()
        {
            try
            {
                richTextBox1.Text = String.Empty;
                var request = "call LoadPosts('" + uname + "')";
                var adapter = conn.Openadapter(request);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                DataRow dataRow = dt.Rows[dt.Rows.Count - 1];
                richTextBox1.Text = dataRow["postTitle"] + "\n" + dataRow["postText"];
            }
            catch
            {

            }
            finally
            {
                conn.Closeconn();
            }
        }
    }
}
