﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kursova_dirty
{
    public partial class Postspage : Form
    {
        public Postspage()
        {
            InitializeComponent();
            LoadPosts();
        }

        ConnectionEstablisher conn = new ConnectionEstablisher();

        private void LoadPosts()
        {
            var request = "call LoadPosts('" + Main.InstanceM.Username + "')";
            var adapter = conn.Openadapter(request);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            listBox1.DataSource = dt;
            listBox1.DisplayMember = "postTitle";
            listBox1.ValueMember = "postID";
        }


        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                int id = int.TryParse(listBox1.SelectedValue.ToString(), out id) ? id : 0;
                var request = "Select postText from kursova.posts Where postID = " + id;
                var command = conn.Openconn(request);
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    richTextBox1.Text = reader.GetString("postText");
                }
            }
            catch
            {
                
            }
            finally
            {
                conn.Closeconn();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var request = "Insert Into kursova.posts (postTitle, postText, user_Uname, user_ID) " +
                "Values ('" + waterMarkTextBox1.Text + "', '', '" + Main.InstanceM.Username + "', " + Main.InstanceM.Userid + ")";
            try
            {
                if (waterMarkTextBox1.Text != String.Empty)
                {
                    conn.Openconn(request);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conn.Closeconn();
            }
            LoadPosts();
            listBox1.SetSelected(listBox1.Items.Count -1, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = int.TryParse(listBox1.SelectedValue.ToString(), out id) ? id : 0;
            var request = "UPDATE `kursova`.`posts` SET `postText`='" + richTextBox1.Text + "' WHERE `postID`='" + id + "'";
            conn.Openconn(request);
            conn.Closeconn();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int id = int.TryParse(listBox1.SelectedValue.ToString(), out id) ? id : 0;
            var request = "DELETE FROM `kursova`.`posts` WHERE `postID`='" + id + "';";
            conn.Openconn(request);
            conn.Closeconn();
            LoadPosts();
        }
    }
}
