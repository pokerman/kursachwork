﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace kursova_dirty
{
    public partial class Profile : Form
    {
        public Profile()
        {
            InitializeComponent();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            LoadData();
        }

        ConnectionEstablisher conn = new ConnectionEstablisher();
        string path = null;
        byte[] byteBLOBData;
        //private string movepath = @"C:\ProgramData\MySQL\MySQL Server 8.0\Uploads\";

        private void LoadData()
        {
            string request = "call sdataload ('" + Main.InstanceM.Username + "')";
            var command = conn.Openconn(request);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                label1.Text = reader.GetString("Uname");
                textBox1.Text = reader.GetString("FName");
                textBox2.Text = reader.GetString("LName");
                textBox4.Text = reader.GetString("PNumber");
                textBox3.Text = reader.GetString("Adress");
                comboBox1.Text = reader.GetString("Gender");
                textBox6.Text = reader.GetString("Age");
                byteBLOBData = (byte[])reader["imhash"];
                var stream = new MemoryStream(byteBLOBData);
                Image image = Image.FromStream(stream);
                pictureBox1.Image = image;
            }
            conn.Closeconn();
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            PictureBox mainpic = new PictureBox();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                mainpic.Image = Image.FromFile(fileDialog.FileName);
                this.path = fileDialog.FileName;
                this.path = this.path.Replace(@"\", "/");
                pictureBox1.Image = Image.FromFile(fileDialog.FileName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string request = "call profiledataupdate('" + Main.InstanceM.Username + "', " + Main.InstanceM.Userid + "," +
            " load_file('" + path + "'), '" + textBox1.Text + "'," +
            " '" + textBox2.Text + "', '" + textBox3.Text + "'," +
            " " + textBox6.Text + ", '" + comboBox1.SelectedItem.ToString() + "', '" + textBox4.Text + "')";
            try
            {
                conn.Openconn(request);
                conn.Closeconn();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Too big Image");
            }
            Main.InstanceM.LoadParent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
