﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace kursova_dirty
{
    public partial class UserCreate : Form
    {
        public UserCreate()
        {
            InitializeComponent();
            Instance1 = this;
        }

        ConnectionEstablisher conn = new ConnectionEstablisher();
        Hasher hash = new Hasher();
        const int minl = 6;
        const int maxl = 18;
        public static UserCreate Instance1;
        private string usercreatename;
        public string Usercreatename
        {
            get
            {
                return this.waterMarkTextBox1.Text;
            }
            set
            {
                this.usercreatename = value;
            }
        }

        //Check username for existance
        private bool UnameCheck()
        {
            string request = "SELECT Uname FROM kursova.user WHERE Uname LIKE" + " '" + this.Usercreatename + "' ";
            var command = conn.Openconn(request);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader.HasRows == true)
                {
                    conn.Closeconn();
                    return false;
                }
            }
            return true;
        }

        //Check for password characters
        private bool PasswordCheck(string password)
        {
            var hasSymbols = new Regex(@"[!#$%&'()*+,-.:;<=>?@[\\\]{}^_`|~]");
            var haskyr = new Regex(@"[А-яёЁ]+");
            if (hasSymbols.IsMatch(password) || haskyr.IsMatch(password))
            {
                return false;
            }
            else
                return true;
        }

        //Main password requirements check
        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            var password = waterMarkTextBox2.Text;
            if (password != waterMarkTextBox3.Text)
            {
                label1.Text = "Passwords not matching";
            }
            else if (!UnameCheck())
            {
                label1.Text = "Username is already taken";
            }
            else if (!PasswordCheck(password))
            {
                label1.Text = @"Password should contain only romal alphabet chars ";
            }
            else if (password.Length < minl || password.Length > maxl)
            {
                label1.Text = "Password shoud be minimum of 6 chars and max of 16";
            }
            else
            {
                var hashedString = hash.Hash(password);
                Register(hashedString);
            }
        }

        //Inserting new user into database
        private void Register(string hashedString)
        {
            string request = "INSERT INTO `kursova`.`user` (`Uname`, `passwordhash`, `role_idrole`) VALUES ('" + this.Usercreatename + "', '" + hashedString + "', '" + "2" + "');";
            conn.Openconn(request);
            MessageBox.Show("Successfully registred!");
            UserCreateData form1 = new UserCreateData();
            form1.ShowDialog();
            this.Close();
            conn.Closeconn();
        }
    }
}
